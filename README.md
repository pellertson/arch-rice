My Arch Rice
============
![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png)

The title is pretty self-explainatory. I use [dotbro](https://github.com/hypnoglow/dotbro) 
to manage my configuration.  Shit's pretty cool.


I also take advantage of [dwm](https://dwm.suckless.org) and [st](https://st.suckless.org),
a light weight window manager and terminal emulator provided by the suckless community, both
available under the MIT License.  See the `LICENSE` file in the `st` and `dwm` folders for 
more details.
