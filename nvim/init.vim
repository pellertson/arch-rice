call plug#begin('~/.config/nvim/plugins')
Plug 'iCyMind/NeoSolarized'
Plug 'cespare/vim-toml'
call plug#end()

" turn on numbered lines and  make them relative
set number
set relativenumber

" self explainatory
colorscheme NeoSolarized
set background=dark " dark | light "

set tabstop=2
set shiftwidth=2

let mapleader=";"

imap <Leader><CR> <C-o>/<++><CR>c4l

" template commands
autocmd FileType html imap ;html <C-o>:-1read ~/.config/nvim/templates/html/start.html<CR>


" python settings
autocmd FileType python set tw=78 sw=4 ts=4 expandtab sts=4 shiftround autoindent
