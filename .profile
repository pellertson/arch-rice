#
# ~/.bash_profile
#

export PATH=$PATH:$HOME/scripts
export TERM='st'
export EDITOR='nvim'
export BROWSER='qutebrowser'
# export BROWSER='firefox'

[[ -f ~/.bashrc ]] && . ~/.bashrc

startx
