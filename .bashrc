#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# get vi mode in bash
set -o vi

# ls aliases
alias ls='ls --color=auto'
alias ll='ls -la --color=auto'

# editing/reloading config files easily
alias eb='vis ~/.bashrc'
#alias ev='vis ~/.config/vis/init.vim'
alias eq='vis ~/.config/qtile/config.py'
alias eqb='vis ~/.config/qutebrowser/config.py'
alias rb='source ~/.bashrc'
alias wifi='sudo netctl stop-all && sudo netctl start Bushido'
alias vi='vis'

# run common apps
alias nb='newsboat'

afetch () {
	git clone https://aur.archlinux.org/$1.git
}


# TODO: give my PS1 some colors
PS1='[\u@\h \W]\$ '

source /etc/profile.d/plan9.sh
